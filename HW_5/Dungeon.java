//Dungeon class for HW 5
//Author: Shayna Bergeron
//Date: 7/11/2018

import java.util.Scanner;//importing scanner for user input

public class Dungeon{
	private Room balcony;
	private Room bedroom1;
	private Room dining;
	private Room bedroom2;
	private Room kitchen;
	private Room northHall;
	private Room southHall;

	/*constructor*/
	public Dungeon(Room balcony, Room bedroom1, Room bedroom2, 
	Room dining, Room kitchen, Room northHall, Room southHall){
		this.balcony = balcony;
		this.bedroom1 = bedroom1;
		this.bedroom2 = bedroom2;
		this.dining = dining;
		this.kitchen = kitchen;
		this.northHall = northHall;
		this.southHall = southHall;
		this.kitchen.setExits(this.northHall,this.southHall,this.bedroom1,this.bedroom2);
		this.balcony.setNorth(dining);
		this.balcony.setEast(northHall);
		this.bedroom1.setEast(bedroom2);
		this.bedroom2.setWest(bedroom1);
		this.dining.setEast(this.kitchen);
		this.northHall.setEast(this.southHall);
		this.southHall.setWest(this.northHall);
		this.northHall.setNorth(this.kitchen);
		this.balcony.setWest(this.kitchen);
		this.bedroom1.setNorth(this.kitchen);
		this.bedroom2.setSouth(this.dining);
	}//end of dungeon constructor
	public Room getRoom0(){
		return this.balcony;
	}
}//end of class