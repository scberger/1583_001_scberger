//Program that creats a game map
//Author: Shayna Bergeron
//Date: 7/1/2018

/*imports of everything needed*/
import java.util.Scanner;

public class Dungeon{
	/*declarations and initializations of variables used*/
	private static int north = 0;
	private static int south = 1;
	private static int east = 2;
	private static int west = 3;
	private static int roomIn = 0; //room the player is currently in

	public static void main(String[] args){
		/*the start of the array for the room entries*/
		String[] dungeonRooms = new String [5];
		dungeonRooms [0] = "You are currently in the main passageway. The exits are located to the north south, east, and west.";
		dungeonRooms [1] = "You are currently in the dining lounge. The exits are located to the north, south, and west.";
		dungeonRooms [2] = "You are currently in the medical room. The exits are located to the north, south and east.";
		dungeonRooms [3] = "You are currently in the bridge. The exists are located to the south, east, and west.";
		dungeonRooms [4] = "You are currently in the engine room. The exits are located to the north, east, and west";
		/*the part of the array for the dungeon exits*/
		int [][] dungeonExits = {{0,4,2,3},{0,4,-1,1},{0,4,1,-1},{-1,1,2,3},{1,-1,2,3}};
		System.out.print("You are currently on the USS Enterprise...type 0 and press enter to continue.");

		/*creating a for loop to loop through the options while switching rooms*/
		for (int i = 5; i > 0; i++){
			/*creating a new scanner for user input*/
			Scanner input = new Scanner(System.in);
			/*for player char entry*/
			char playerChoice = input.nextLine().charAt(0);
			System.out.println(dungeonRooms[roomIn]);
			System.out.printf("%n%s%n%s%n%s%n%s%n%s%n%s%n", "Which room would you like to enter, type a coordinate.", "n: Move north", "s: Move south", "e: Move east", "w: Move west", "q: Quit the game");
			/*creating a muti-selection statement for user entries*/
			if (playerChoice == 'n' || playerChoice == 'N'){
				if (dungeonExits [roomIn][north] < 0){
					System.out.println("There is no exit that way.");
				}
				else
					roomIn = dungeonExits[roomIn][north];
			}
			else if (playerChoice == 's' || playerChoice == 'S'){
				if (dungeonExits [roomIn][south] < 0){
					System.out.println("There is no exit that way.");
				}
				else
					roomIn = dungeonExits[roomIn][south];
			}
			else if (playerChoice == 'e' || playerChoice == 'E'){
				if (dungeonExits [roomIn][east] < 0){
					System.out.println("There is no exit that way.");
				}
				else
					roomIn = dungeonExits[roomIn][east];
			}
			else if (playerChoice == 'w' || playerChoice == 'W'){
				if (dungeonExits [roomIn][west] < 0){
					System.out.println("There is no exit that way.");
				}
				else
					roomIn = dungeonExits[roomIn][west];
			}
			else if (playerChoice == 'q' || playerChoice == 'Q'){
				System.out.println("You have exited vessel.");
				break;
			}
			else{
				System.out.println("Invalid entry, try again!");
			}  
		}//end of loop
	}//end of main method
}//end of class method